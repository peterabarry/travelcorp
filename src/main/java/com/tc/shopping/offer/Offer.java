package com.tc.shopping.offer;


import com.tc.shopping.item.DiscountedLineItem;
import com.tc.shopping.item.ProductLineItem;

import java.util.List;

/**
 * An Offer represents the POTENTIAL for Discounts.
 * Given a List of ProductLineItem's can generate a list of discounts
 */

@FunctionalInterface
public interface Offer {

    List<DiscountedLineItem> discount(List<ProductLineItem> lineItems);
}

package com.tc.shopping.offer;


import com.tc.shopping.item.DiscountedLineItem;
import com.tc.shopping.item.LineItems;
import com.tc.shopping.item.ProductLineItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class XForYOffer implements Offer {

    private final String item;
    private final int x;
    private final int y;

    public XForYOffer(Map<String, Object> params) {
        this.item = (String) params.get("Item");
        this.x = Integer.valueOf((String) params.get("X"));
        this.y = Integer.valueOf((String) params.get("Y"));
    }

    private String describe() {
        return x + " for " + y;
    }


    @Override
    public List<DiscountedLineItem> discount(List<ProductLineItem> lineItems) {
        List<DiscountedLineItem> discounts = new ArrayList<>();
        List<ProductLineItem> validItems = lineItems
                .stream()
                .filter((pi) -> pi.getDescription().equals(this.item)).collect(Collectors.toList());
        long timesToApplyDiscount = validItems.size() / x;
        int start = 0;

        /*no elegant 'partition/chunk' in java 8 so use a while loop rather than another library*/
        while (timesToApplyDiscount-- > 0) {
            List<ProductLineItem> discountableLineItems = validItems.subList(start,  start += x);
            double price = -(x - y) * discountableLineItems.get(0).getValue();
            discounts.add(LineItems.discounted(discountableLineItems, this.describe(), price));

    }
        return discounts;
    }
}

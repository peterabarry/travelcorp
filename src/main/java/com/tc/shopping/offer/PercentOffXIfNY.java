package com.tc.shopping.offer;

import com.tc.shopping.item.DiscountedLineItem;
import com.tc.shopping.item.LineItem;
import com.tc.shopping.item.LineItems;
import com.tc.shopping.item.ProductLineItem;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PercentOffXIfNY implements Offer {

    private final String itemOff;
    private final String itemDeal;
    private final int itemDealQuant;
    private final int percentOff;

    public PercentOffXIfNY(Map<String, Object> params) {
        this.itemOff = (String) params.get("ItemOff");
        this.itemDeal = (String) params.get("ItemDeal");
        this.percentOff = Integer.valueOf((String) params.get("PercentOff"));
        this.itemDealQuant = Integer.valueOf((String) params.get("ItemDealQuant"));
    }

    private String describe() {
        return new StringBuilder()
                .append(percentOff)
                .append("% off")
                .toString();
    }

    @Override
    public List<DiscountedLineItem> discount(List<ProductLineItem> lineItems) {

        List<LineItem> activationItems = lineItems
                .stream()
                .filter((pi) -> pi.getDescription().equals(this.itemDeal)).collect(Collectors.toList());

        long count = activationItems.size();
        long maxTimesToApplyDiscount = count / itemDealQuant;

        return lineItems
                .stream()
                .filter((item) -> item.getDescription().equals(this.itemOff))
                .limit(maxTimesToApplyDiscount)
                .map((item)-> LineItems.discounted(item, this.describe(), item.getValue()*-this.percentOff/100 ))
                .collect(Collectors.toList());

    }

}

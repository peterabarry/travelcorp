package com.tc.shopping;

import com.tc.shopping.item.DiscountedLineItem;
import com.tc.shopping.item.LineItem;
import com.tc.shopping.item.ProductLineItem;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A Bill is a summable list of line items.
 */
public class Bill {
    private final List<LineItem> lineItems;

    private Bill(List<LineItem> pricedItems) {
        this.lineItems = pricedItems;
    }

    public List<LineItem> getLineItems() {
        return this.lineItems;
    }

    public double getTotal() {
        return this.lineItems.stream().collect(Collectors.summingDouble(LineItem::getValue));
    }

    /*The construction knits the discounted line items to appear after the last item they apply to so its easier
    * to read in the receipt*/
    public static Bill from(List<ProductLineItem> productLineItems, List<DiscountedLineItem> discountedItems) {
        List<LineItem> billContents = new LinkedList<>();
        billContents.addAll(productLineItems);

        for (DiscountedLineItem discountedLineItemLine : discountedItems) {
            List<ProductLineItem> ordered = discountedLineItemLine.getItems();
            LineItem lineItem = ordered.get(ordered.size() - 1);
            int found = -1;
            //not optimized, but would be when/if needed.
            for (int i = 0; i < billContents.size(); i++) {
                if (lineItem.equals(billContents.get(i))) {
                    found = i;
                    break;
                }

            }
            billContents.add(found + 1, discountedLineItemLine);
        }
        return new Bill(billContents);
    }
}

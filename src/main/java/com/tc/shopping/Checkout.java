package com.tc.shopping;

import com.tc.shopping.item.DiscountedLineItem;
import com.tc.shopping.item.LineItems;
import com.tc.shopping.item.ProductLineItem;
import com.tc.shopping.offer.Offer;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A Checkout prices baskets which result in bills. It has a physical domain corollary.
 */
public class Checkout {
    private final PriceList priceList;
    private final Set<Offer> offers;

    private Checkout(PriceList priceList, Set<Offer> offers) {
        this.priceList = priceList;
        this.offers = offers;
    }

    public Bill price(Basket basket) {
        return Checkout.price(this.priceList, this.offers, basket);
    }

    //the OO part is just for contextual convenience, the core is just functional:
    public static Bill price(PriceList prices, Set<Offer> offers, Basket basket) {

        List<ProductLineItem> productLineItems = priceProductsByPriceList(prices, basket);

        return Bill.from(
                productLineItems,
                generateApplicableDiscounts(offers, productLineItems)
        );
    }

    private static List<DiscountedLineItem> generateApplicableDiscounts(Set<Offer> offers, List<ProductLineItem> productLineItems) {
        return offers.stream()
                .flatMap((offer) -> offer.discount(productLineItems).stream())
                .collect(Collectors.toList());
    }

    private static List<ProductLineItem> priceProductsByPriceList(PriceList prices, Basket basket) {
        return basket.getContents()
                .stream()
                .map(item -> LineItems.priced(item, prices.getPrice(item).orElseThrow(() -> new RuntimeException(item + " has no price."))))
                .collect(Collectors.toList());
    }

    public static Checkout from(PriceList priceList, Set<Offer> offers) {
        return new Checkout(priceList, offers);
    }
}

package com.tc.shopping;


import java.util.List;

/**
 * A Basket is a list of items which has a physical domain corollary.
 *
 * We deliberately keep a list and retain duplicates because it maps closer to the physical reality and
 * is easier to apply offers against rather than trying to summarize into a bag of item->count.
 *
 * Products should probably be more than Strings, but in this simple example just not needed.
 */
public class Basket {

    private final List<String> contents;

    private Basket(List<String> contents) {
        this.contents = contents;
    }

    public List<String> getContents() {
        return this.contents;
    }

    public static Basket from(List<String> basket) {
        return new Basket(basket);
    }
}

package com.tc.shopping.printer;

import com.tc.shopping.Bill;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;


public class NoFluffReceiptPrinter implements ReceiptPrinter {

    private static final String FORMAT_ITEM = "%-10s %8.2f%n";
    private static final String BREAKER = "%19s%n";
    private static final String FORMAT_TOTAL = "%-10s  £  %4.2f"; //hard code GBP for now.

    @Override
    public void printReceipt(Bill bill, OutputStream target) {
        StringBuilder builder = new StringBuilder();
        bill.getLineItems().stream().map((i) -> String.format(FORMAT_ITEM, i.getDescription(), i.getValue())).forEach(builder::append);
        builder.append(String.format(BREAKER, "----"));
        builder.append(String.format(FORMAT_TOTAL, "Total:", bill.getTotal()));
        try {
            target.write(normalizeLE(builder.toString()).getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException("Out of paper, could not print receipt. :-) ", e);
        }
    }

    /*Just for ease in testing */
    private String normalizeLE(String toNormalize) {
        return toNormalize.replaceAll("\\r\\n", "\n");
    }
}

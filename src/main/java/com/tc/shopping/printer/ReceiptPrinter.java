package com.tc.shopping.printer;

import com.tc.shopping.Bill;

import java.io.OutputStream;


public interface ReceiptPrinter {

    void printReceipt(Bill bill, OutputStream stream);

}

package com.tc.shopping.printer;


import com.tc.shopping.Bill;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;

public class Printers {

    /*Just to keep the RecieptPrinters interface clean while allowing a simple form to String*/
    public static String printNoFluffReceipt(Bill bill){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        new NoFluffReceiptPrinter().printReceipt(bill, baos);
        return new String(baos.toByteArray(), Charset.forName("UTF-8"));
}}

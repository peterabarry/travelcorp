package com.tc.shopping.item;


public class AbstractLineItem implements LineItem {

    private final String description;
    private final double value;

    protected AbstractLineItem(String description, double value) {

        this.description = description;
        this.value = value;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getValue() {
        return value;
    }
}

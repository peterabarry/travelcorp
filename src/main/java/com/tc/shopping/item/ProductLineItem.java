package com.tc.shopping.item;


public class ProductLineItem extends AbstractLineItem implements  LineItem {

    protected ProductLineItem(String description, double value) {
        super(description, value);
    }
}

package com.tc.shopping.item;

import java.util.Collections;
import java.util.List;

public class DiscountedLineItem extends AbstractLineItem implements LineItem {

    private final List<ProductLineItem> items;

    protected DiscountedLineItem(List<ProductLineItem> items, String description, double discountedAmount) {
       super("*  "+description, discountedAmount);
       this.items = items;
    }

    protected DiscountedLineItem(ProductLineItem item, String description, double price) {
      this( Collections.singletonList(item), description, price);
    }

    public List<ProductLineItem> getItems() {
        return items;
    }


}

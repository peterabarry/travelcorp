package com.tc.shopping.item;

import java.util.List;

/*A single factory-like API to create line items*/
public class LineItems {

    public static ProductLineItem priced(String description, double value){
        return new ProductLineItem(description, value);
    }

    public static DiscountedLineItem discounted(List<ProductLineItem> affected, String description, double value){
        return new DiscountedLineItem(affected, description, value);
    }

    public static DiscountedLineItem discounted(ProductLineItem affected, String description, double value){
        return new DiscountedLineItem(affected, description, value);
    }
}

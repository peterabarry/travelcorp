package com.tc.shopping.item;


public interface LineItem {

    String getDescription();

    double getValue();
}

package com.tc.shopping;

import java.util.Map;
import java.util.Optional;

/**
 * A PriceList is a list mapping the products in inventory to their unit prices. It has a physical domain corollary.
 */
public class PriceList {

    final private Map<String, Double> priceList;

    private PriceList(Map<String, Double> priceList){
        this.priceList = priceList;
    }

    public Optional<Double> getPrice(String item) {
        return Optional.ofNullable(priceList.get(item));
    }

    public static PriceList from(Map<String, Double> pricesList) {
        return new PriceList(pricesList);
    }
}

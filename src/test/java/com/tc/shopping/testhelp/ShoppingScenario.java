package com.tc.shopping.testhelp;

import com.esotericsoftware.yamlbeans.YamlReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ShoppingScenario {

    public static List<ShoppingScenario> loadAll() {
        List<ShoppingScenario> cases = new ArrayList<>();
        try {
            URL url = ClassLoader.getSystemResource("specs"); //use this as anchor to avoid scanning cp.
            DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get(url.toURI()));

            paths.forEach(path -> {
                try{
                    YamlReader reader = new YamlReader(new String(Files.readAllBytes(path), Charset.forName("UTF-8")));
                    reader.getConfig().setPropertyElementType(ShoppingScenario.class, "Offers", OfferSpec.class);
                    cases.add( reader.read(ShoppingScenario.class));
                } catch (IOException e) {
                    throw new RuntimeException("Could not load test data from"+ path, e);
                }
            });
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("Could not load test data", e);
        }
        return cases;
    }

    public String Description;
    public Map<String, String> Prices;
    public List<OfferSpec> Offers;
    public List<String> Basket;
    public String ReceiptExpected;

    @Override
    public String toString() {
        return Description;
    }
}

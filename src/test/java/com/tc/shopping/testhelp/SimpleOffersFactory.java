package com.tc.shopping.testhelp;


import com.tc.shopping.offer.Offer;
import com.tc.shopping.offer.PercentOffXIfNY;
import com.tc.shopping.offer.XForYOffer;

import java.util.HashMap;
import java.util.Map;

/*
* As we do not know the requirements, we keep things simple rather than use a
* DI framework ( Guice, Spring, Picocontainer).
* */
public class SimpleOffersFactory {

    Map<String, Class<? extends Offer>> offerTypeImpls = new HashMap<String, Class<? extends Offer>>(){{
            put("% Off X if N Y", PercentOffXIfNY.class);
            put("X For Y", XForYOffer.class);
    }};

    public Offer create(String type,Map<String, Object> params) {
        try {
            return offerTypeImpls.get(type).getConstructor(Map.class).newInstance(params);
        } catch (Exception e) {
           throw new RuntimeException(e);
        }
    }
}

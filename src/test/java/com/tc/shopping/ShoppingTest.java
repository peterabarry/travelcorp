package com.tc.shopping;


import com.tc.shopping.offer.Offer;
import com.tc.shopping.printer.Printers;
import com.tc.shopping.testhelp.OfferSpec;
import com.tc.shopping.testhelp.ShoppingScenario;
import com.tc.shopping.testhelp.SimpleOffersFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RunWith(Parameterized.class)
public class ShoppingTest {

    private final SimpleOffersFactory offersFactory;

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Iterable<Object[]> data() {
        return ShoppingScenario.loadAll().stream().map(item -> new Object[]{item}).collect(Collectors.toList());
    }

    ShoppingScenario scenario;

    public ShoppingTest(ShoppingScenario scenario) {
        this.scenario = scenario;
        this.offersFactory = new SimpleOffersFactory();
    }

    @Test
    public void checkSpecification(){
        PriceList prices = PriceList.from(coerceToMapOfDoubles(scenario.Prices));
        List<OfferSpec> offersSpecs = scenario.Offers == null ? new ArrayList<>() : scenario.Offers;
        Set<Offer> offers= offersSpecs.stream()
                .map((offerSpec) -> offersFactory.create(offerSpec.Type, offerSpec.Params))
                .collect(Collectors.toSet());
        Checkout checkout = Checkout.from(prices, offers);
        Bill bill = checkout.price(Basket.from(scenario.Basket));
        Assert.assertEquals(scenario.Description, scenario.ReceiptExpected,Printers.printNoFluffReceipt(bill) );
    }

    /*The YAML parser in use is not that smart*/
    private Map<String, Double> coerceToMapOfDoubles(Map<String, String> prices) {
        return prices.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        e -> Double.valueOf(e.getValue())));
    }

}

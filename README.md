
### Development prerequisites:
1. Maven
2. Java 8

### Steps to run locally:
1. mvn package

### Environments:
* I developed this on Windows ( though I do have and use both Windows and Linux ). 
* This was tested and developed on Windows so on Linux YMMV.

### FEEDBACK FROM CANDIDATE:

I tried to keep things simple but also useful. I prefer to write in a data-driven style both in terms of test cases and in terms of the way I handle things which are *very likely* to change. Something a bit like the book "Growing Object Oriented Software Guided By tests"

I would call this ATDD (acceptance test driven development) rather than TDD. The intention of this style is to have a very end goal driven, fast testing capability that can act as a safety net and guide while doing early stage / new development. Once the code base starts to stabilize ( inc requirements), then I would start to move toward a more unit level TDD approach. 

### SOLUTION:

In this case I provide a simple yaml based DSL(ish) to allow:
*  Unit Price List to be uploaded as data inputs.
*  Offers to be data driven and can be easily changed / added.
*  Full reciept comparison / validation.




